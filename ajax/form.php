<?php
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if( !empty($_REQUEST['mode']) && !empty($_REQUEST['phone']) ) {
    CModule::IncludeModule('iblock');

    $el = new CIBlockElement;
    // общие поля
    $arFields = Array(
        "IBLOCK_SECTION_ID" => false,
        'DATE_ACTIVE_FROM' => ConvertTimeStamp(time()+CTimeZone::GetOffset(), "FULL"),
        "PROPERTY_VALUES"=> array(
            'PHONE' => $_REQUEST['phone']
        ),
        "NAME"           => $_REQUEST['phone'],
        "PREVIEW_TEXT"   => $_REQUEST['mode'],
    );

    // обратный звонок
    if($_REQUEST['mode'] == 'return_call' ) {
        $arFields['IBLOCK_ID'] = FORM_CALLME_IBLOCK;
        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_CALLME_IBLOCK, 'STATUS', 'pending')
        );
    }

    // вызов замерщика
    if($_REQUEST['mode'] == 'master_call' ) {
        $arFields['IBLOCK_ID'] = FORM_MASTER_IBLOCK;
        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_MASTER_IBLOCK, 'STATUS', 'pending')
        );
    }

    // связаться с директором
    if($_REQUEST['mode'] == 'director_call' ) {
        $arFields['IBLOCK_ID'] = FORM_DIRECTOR_IBLOCK;

        if(!empty($_REQUEST['name']))
            $arFields['PREVIEW_TEXT'] = $_REQUEST['name'];

        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_DIRECTOR_IBLOCK, 'STATUS', 'pending')
        );
    }

    // консультация
    if($_REQUEST['mode'] == 'consult_call' || $_REQUEST['mode'] == 'helper' ) {
        $arFields['IBLOCK_ID'] = FORM_CONSULT_IBLOCK;
        if(!empty($_REQUEST['name']))
            $arFields['PREVIEW_TEXT'] = $_REQUEST['name'];

        if(!empty($_REQUEST['comment']))
            $arFields['DETAIL_TEXT'] = $_REQUEST['comment'];

        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_CONSULT_IBLOCK, 'STATUS', 'pending')
        );
    }

    // вопросы
    if($_REQUEST['mode'] == 'questions_call' ) {
        $arFields['IBLOCK_ID'] = FORM_QUESTION_IBLOCK;
        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_QUESTION_IBLOCK, 'STATUS', 'pending')
        );
    }

    // заказ двери
    if($_REQUEST['mode'] == 'orderdoor' ) {
        $arFields['IBLOCK_ID'] = FORM_ORDER_IBLOCK;
        $arFields['NAME'] .= ' (' . $_REQUEST['model'] . ')';
        $arFields['PREVIEW_TEXT'] = $_REQUEST['model'];
        $arFields['DETAIL_TEXT'] = $_REQUEST['comment'];
        $arFields['PROPERTY_VALUES']['STATUS'] = array(
            'VALUE' => FormsEvent::getEnum(FORM_ORDER_IBLOCK, 'STATUS', 'pending'),
        );
        $arFields['PROPERTY_VALUES']['DOOR'] = array(
            'VALUE' => $_REQUEST['model_id'],
        );
        $res = CIBlockElement::GetList(array(), array('ID'=>$_REQUEST['model_id']), false, false, array('PROPERTY_PRICE'));
        $element = $res->Fetch();
        $arFields['PROPERTY_VALUES']['PRICE'] = $element['PROPERTY_PRICE_VALUE'];
    }

    $RESULT_ID = $el->Add($arFields);

}