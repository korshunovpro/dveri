<?php
//iblocks
define('DOOR_CATALOG_IBLOCK', 3);
define('DOOR_FACTORY_IBLOCK', 6);

// forms event
define('FORM_CALLME_IBLOCK',    8);
define('FORM_DIRECTOR_IBLOCK',  10);
define('FORM_ORDER_IBLOCK',     9);
define('FORM_MASTER_IBLOCK',    11);
define('FORM_CONSULT_IBLOCK',   12);
define('FORM_QUESTION_IBLOCK',  FORM_CONSULT_IBLOCK);

AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("FormsEvent", "sendEvent"));

/**
 * Class FormsEvent
 */
class FormsEvent
{
    // ID значения из списка статусов
    public static function getEnum($IBLOCK_ID, $CODE = 'STATUS', $XML_ID = 'pending') {
        CModule::IncludeModule('iblock');
        $statusEnum = CIBlockPropertyEnum::GetList(array(), array('IBLOCK_ID'=>$IBLOCK_ID, 'CODE'=>$CODE));
        $aResult = array();
        while($aEnum = $statusEnum->GetNext()) {
            $aResult[$aEnum['XML_ID']] = $aEnum['ID'];
        }
        return $aResult[$XML_ID];
    }

    // отправка уведомления на email
    function sendEvent(&$arFields)
    {
        // общие поля
        $arEventFields = array(
            "PHONE" => $arFields["PROPERTY_VALUES"]['PHONE'],
        );

        if($arFields['IBLOCK_ID'] == FORM_CALLME_IBLOCK) {
            CEvent::SendImmediate("DD_FORM_CALLME", SITE_ID, $arEventFields);
        }

        if($arFields['IBLOCK_ID'] == FORM_MASTER_IBLOCK) {
            CEvent::SendImmediate("DD_FORM_MASTER", SITE_ID, $arEventFields);
        }

        if($arFields['IBLOCK_ID'] == FORM_DIRECTOR_IBLOCK) {
            $arEventFields['NAME'] = $arFields['PREVIEW_TEXT'];
            CEvent::SendImmediate("DD_FORM_CALLME", SITE_ID, $arEventFields);
        }

        if($arFields['IBLOCK_ID'] == FORM_CONSULT_IBLOCK) {
            $arEventFields['NAME'] = $arFields['PREVIEW_TEXT'];
            $arEventFields['COMMENT'] = $arFields['DETAIL_TEXT'];
            CEvent::SendImmediate("DD_FORM_CONSULT", SITE_ID, $arEventFields);
        }

        if($arFields['IBLOCK_ID'] == FORM_QUESTION_IBLOCK) {
            CEvent::SendImmediate("DD_FORM_QUESTION", SITE_ID, $arEventFields);
        }

        if($arFields['IBLOCK_ID'] == FORM_ORDER_IBLOCK) {
            $arEventFields['COMMENT'] = $arFields['DETAIL_TEXT'];
            $arEventFields['DOOR'] = $arFields['PREVIEW_TEXT'];
            $arEventFields['PRICE'] = $arFields["PROPERTY_VALUES"]['PRICE'];
            CEvent::SendImmediate("DD_FORM_ORDER", SITE_ID, $arEventFields);
        }

    }
}

/**
 * Class DoorCompare
 */
class DoorCompare
{
    public static function getCountItem()
    {
        return count($_SESSION['CATALOG_COMPARE_LIST'][DOOR_CATALOG_IBLOCK]['ITEMS']);
    }

    public static function checkID($ID)
    {
        return array_key_exists($ID, ($_SESSION['CATALOG_COMPARE_LIST'][DOOR_CATALOG_IBLOCK]['ITEMS']));
    }
}

/*-----CUSTOM URL-----*/
AddEventHandler("main", "OnBeforeProlog", array("CustomFormatUrl", "addSectionUrlRewrite"));
//AddEventHandler("main", "OnBeforeProlog", array("CustomFormatUrl", "addFactoryUrlRewrite"));
class CustomFormatUrl
{
    /**
     * Добавляет чпу для каталога, от корня сайта
     * по Разделам дверей 1го уровня /раздел-дверей
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public static function addSectionUrlRewrite()
    {
        CModule::IncludeModule('iblock');

        $res = CIBlockSection::GetList(
            array(),
            array('IBLOCK_ID'=>DOOR_CATALOG_IBLOCK, 'DEPTH_LEVEL'=>1)
        );

        $arRules = Bitrix\Main\UrlRewriter::getList(SITE_ID);
        while($el = $res->GetNext(false)) {

            $ruleAdd = true;
            $condition = '#^/' . $el['CODE'] . '/#';

            foreach ($arRules as $rule) {
                if($rule['CONDITION'] == $condition) {
                    $ruleAdd = false;
                    break;
                }
            }

            if($ruleAdd) {
                Bitrix\Main\UrlRewriter::add(SITE_ID,
                    array(
                        'CONDITION' => $condition,
                        'RULE' => 'SECTION_CODE=' . $el['CODE'],
                        'ID' => 'bitrix:catalog',
                        'PATH' => '/catalog/index.php',
                        'SORT' => 10,
                    )
                );
            }
        }
    }

    /**
     * Добавляет чпу для каталога, от корня сайта
     * по Разделам дверей 1го уровня /раздел-дверей
     * @throws \Bitrix\Main\ArgumentNullException
     */
    public static function addFactoryUrlRewrite()
    {
        CModule::IncludeModule('iblock');

        $res = CIBlockElement::GetList(
            array(),
            array('IBLOCK_ID'=>DOOR_FACTORY_IBLOCK, 'ACTIVE'=>'Y')
        );

        $arRules = Bitrix\Main\UrlRewriter::getList(SITE_ID);
        while($el = $res->GetNext(false)) {

            $ruleAdd = true;
            $condition = '#^/' . $el['CODE'] . '/#';

            foreach ($arRules as $rule) {
                if($rule['CONDITION'] == $condition) {
                    $ruleAdd = false;
                    break;
                }
            }

            if($ruleAdd) {
                Bitrix\Main\UrlRewriter::add(SITE_ID,
                    array(
                        'CONDITION' => $condition,
                        'RULE' => 'ELEMENT_CODE=' . $el['CODE'],
                        'ID' => 'bitrix:catalog',
                        'PATH' => '/catalog/factory.php',
                        'SORT' => 10,
                    )
                );
            }
        }
    }

}
