<?php include('header.php'); ?>

<article id="compare">

	<div class="compare-bg">
	  	<div class="specs">
	  		<div class="special"><div class="hidden-xs hidden-md hidden-sm point"><span class="green underline">Показывать только<br>отличающиеся</span></div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Производитель</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Страна</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Покрытие</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Наличие</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Остекление</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Производитель</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Страна</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Покрытие</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Наличие</div></div>
			<div class="spec"><div class="hidden-xs hidden-md hidden-sm point">Остекление</div></div>
			<div class="spec double"><div class="hidden-xs hidden-md hidden-sm point">Краткое описание</div></div>
		</div>
	</div>

	<div class="container">
		<h1 class="title">Таблица сравнения</h1>

		<div id="slider" class="compare">
		  <div class="container">
		  	<div class="row">
		  		<div class="hidden-xs hidden-md col-md-1 for-arrows"></div>
		  		<div class="col-xs-12 col-md-10">
		  		<div class="row">
			  		<div class="carousel">
			  		<?php for ($i=1; $i < 3; $i++) { ?>
				  		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 element">
				  			<div class="inner">
					  			<div class="door_spec">
									<div class="image">
					  				<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
						  			</div>
						  			<div class="description blue-fill top">
						  				<span class="underline name">Форпост <?php if($i % 2 == 0){ echo 'супер в две строки';} echo ($i);?></span><br>
						  				<span class="vendor">Производитель «Торекс»</span><br>
						  				<span class="price">1<?php echo ($i % 9)+1;?>000 Р</span><br>
						  				<span class="underline compare_del" onclick="$(this).parent().parent().parent().fadeOut()">Удалить из сравнения</span>
						  			</div>
								</div>
							</div>
							<div class="specs">
				  				<div class="spec">«Дверлайн шпонированные»</div>
				  				<div class="spec">Россия</div>
				  				<div class="spec">шпонированное</div>
				  				<div class="spec">под заказ от 8 до 14 дней</div>
				  				<div class="spec">остекление отсутствует</div>
				  				<div class="spec">«Дверлайн шпонированные»</div>
				  				<div class="spec">Россия</div>
				  				<div class="spec">шпонированное</div>
				  				<div class="spec">под заказ от 8 до 14 дней</div>
				  				<div class="spec">остекление отсутствует</div>
				  				<div class="spec double">Краткое описание в несколько строчек для каждой двери</div>
				  			</div>
				  		</div>
			  		<?php } ?>
			  		</div>
		  		</div>
		  		</div>
		  		<div class="hidden-xs hidden-md col-md-1 for-arrows"></div>
		  	</div>
		  </div>
		</div>
	</div>
	
</article>

<?php include('footer.php'); ?>
