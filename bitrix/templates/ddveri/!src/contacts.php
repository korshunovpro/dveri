<?php include('header.php'); ?>

<article>
	<div class="container">
		<h1 class="title">Контактная информация</h1>
		<div class="phone_icon">
			<span class="bold">8 800 750 46 47</span> <span class="call-me underline">Перезвоните мне</a>
		</div>
		<div class="mail_icon">
			<a href="mailto:" class="bold underline">chel@d-dveri74.ru</a>
		</div>
		<div class="place_icon">
			г. Челябинск, ул. Ярославная 1/2
		</div>

		<div class="map">
			<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=Vc1YBzjSO7bekkI7tM_giyJoPqsEeUNX&width=&height=450"></script>
		</div>
	</div>
</article>

<?php include('footer.php'); ?>
