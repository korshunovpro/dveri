<?php include('header.php'); ?>
	<div class="container">
		<div class="col-xs-12 col-md-12" id="filter">
		    <form action="">
				<div class="clearfix categories">
					<input type="button"  class="active inline-button button gray-fill" value="Входные двери">
					<input type="button"  class="button inline-button gray-fill" value="Межкомнатные двери">
					<input type="button" class="button inline-button gray-fill" value="Противопожарные двери">
					<input type="text" class="blue search button" placeholder="Поиск">
				</div>
				<div class="clearfix filters">
					<span>Цена: </span>
						<input type="text" id="priceFrom" class="purple_rounded button" readonly>
						<input type="text"id="priceTo" class="purple_rounded button" readonly>
						<div class="slider">
							<div id="price-range">
					          <div class="from"></div>
					          <div class="to"></div>
					        </div>
						</div>
					<div class="clearfix"></div>
					<span>Производители: </span>
						<a href="" class="bordered button">Аргус</a>
						<a href="" class="bordered button disabled">Торекс</a>
						<a href="" class="bordered button active">Супер доорс</a>
						<a href="" class="bordered button">Аргус</a>
						<a href="" class="bordered button">Бульдорс</a>
						<a href="" class="bordered button">Торекс</a>
					<div class="clearfix"></div>
					<span>Покрытия: </span>
						<a href="" class="bordered button">Ламинированные</a>
						<a href="" class="bordered button">Пластик</a>
						<a href="" class="bordered button">Шпон</a>
				</div>
				<br>
				<div class="adds_search orange underline">Расширенный поиск</div>
				<div class="green-fill fix-button">Подобрать</div>
			</form>
		</div>

		<div class="col-xs-12 col-md-12 sorting">
			<div class="left">Сортировка:</div>
			<div class="right">
				<span class="expensive">
					<span class="dashed purple">Сначала дороже</span>
				</span>
				<span class="cheap">
					<span class="dashed green">Сначала дешевле</span>
				</span>
			</div>
		</div>

	</div>

	<div class="container" id="catalog">
		<div class="row">
			<?php for ($i=1; $i < 7; $i++) { ?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="door">
						<div class="image">
		  				<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
		  			</div>
		  			<div class="description blue-fill top">
		  				<span class="underline name">Форпост <?php echo ($i);?></span><br>
		  				<span class="vendor">Производитель «Торекс»</span><br>
		  				<span class="price">1<?php echo ($i % 9)+1;?>000 Р</span><br>
		  				<span class="underline compare">Добавить к сравнению</span>
		  			</div>
					</div>
				</div>
			<?php } ?>
		</div>
		<div class="show-more button green-fill">Посмотреть еще 4</div>
	</div>

<div class="blue-line"></div>

	<article>
		<div class="container">
			<h1 class="title">Текстовая страница</h1>
			<div class="entry">
				<p><b>Все модели межкомнатных дверей доступны в 3-х вариациях (одностворчатая, двустворчатая, раздвижная), фурнитураи комплектующие в стоимость не входят.</b></p>

				<p>Каждая студия выбирает свой стиль работы с рыбой. Автор считает, что все тексты нужно писать самому дизайнеру (правая колонка). У нас в студии принято составлять такую рыбу, чтобы она сама по себе вызывала желание принять дизайн. Поэтому очень часто рыба становится частью готового проекта — так она нравится клиенту.</p>

				<p class="quote">Текст ни о чем (пример) - хороший способ попытатсья сосредоточить заказчика на дизайне, а не на чем-то еще.</p>

				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<img src="images/review_example.jpg"  alt="">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
						<img src="images/review_example.jpg"  alt="">
					</div>
				</div>

				<ul class="list">
					<li><span class="number">1.</span>  Высококвалифицированные, опрятные монтажники с опытом от 3-х лет.
					<li><span class="number">2.</span> Профессиональный инструмент чтобы минимизировать уровень шума, количество пыли и время монтажа.
					<li><span class="number">3.</span> Уборка мусора после монтажа (убираем за собой помещение, складываем мусор в мешки).
				</ul>
			</div>
		</div>
	</article>

<?php include('footer.php'); ?>