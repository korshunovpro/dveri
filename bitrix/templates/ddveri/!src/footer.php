	<footer>	
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-5">
						<h3>
							Контактная информация
						</h3>
						<ul>
							<li class="phone_icon"><span class="bold">8 800 750 46 47</span> <span class="call-me white underline">Перезвоните мне</span>
							<li class="mail_icon"><a href="mailto:" class="bold white underline">chel@d-dveri74.ru</a>
							<li class="place_icon">г. Челябинск, ул. Ярославная 1/2 <a href="/contacts.php" class="white underline">Посмотреть на карте</a>
							<li class="director_icon"><span class="director bold green underline">Связаться с директором</span>
						</ul>
					</div>
					<div class="col-xs-12 col-md-3">
						<h3>Карта сайта</h3>
						<ul>
							<li class="text"><a href="" class="white underline">Монтаж и доставка</a>
							<li class="text"><a href="" class="white underline">Гарантия</a>
							<li class="text"><a href="" class="white underline">О компании</a>
						</ul>
					</div>
					<div class="col-xs-12 col-md-4">
						<form action="" class="smallform">
							<div class="after-send">
								В ближайшее время<br>с вами свяжется<br>наш специалист
							</div>
							<div class="before-send">
								<div class="bold title">Остались вопросы?</div>
								<p>Мы перезвоним в<br>течение 15 минут</p>
								<input type="hidden" name="mode">
								<input type="text" class="button" name="phone" id="phone" placeholder="Введите номер телефона">
								<input type="submit" class="button green-fill" value="Отправить">
							</div>
						</form>
					</div>
				</div>
			</div>
	</footer>

	<div class="line bold">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-9">
					© 2014 Добрые двери
				</div>
				<div class="col-xs-12 col-md-3">
					<div class="right">
					Создание сайта - <a class="white underline" href="http://1jam.ru">Jam</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include('forms.php'); ?>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <!-- Latest compiled and minified JavaScript -->
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- jQuery Easing -->
	<script src="jquery.easing.js"></script>
	<script src="lightbox.min.js"></script>
	<!-- Slick plugin -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.min.js"></script>
	<!-- Input Mask plugin -->
	<script src="jquery.inputmask.js" type="text/javascript"></script>
	<!-- Site scripts -->
	<script src="scripts.js"></script>
  </body>
</html>