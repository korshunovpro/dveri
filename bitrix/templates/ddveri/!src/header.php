<!DOCTYPE html>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Добрые двери</title>
    <!-- Bootstrap -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <meta id="viewport" name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1">
  <meta name="format-detection" content="telephone=no">

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.css"/>

	<style>
		@import url(http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=cyrillic,latin);
		@import url(http://fonts.googleapis.com/css?family=Exo+2:100,200,400,300,200,500,600&subset=latin,cyrillic);
	</style>

	<!-- Stylesheet -->
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="lightbox.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <header>
    <div class="container">
    <div class="row">
   		<div class="col-xs-12 hidden-sm hidden-md hidden-lg to-mobile">
			<a href="/mobile.php" class="white underline"style="line-height:41px">Перейти в полную версию сайта?</a>
   		</div>
    	<div class="col-xs-12 col-md-6">
    		<a class="white" href="/"><div class="white logo"><span class="text">Добрые двери</span></div></a>
	    	<div class="hidden-xs description">Входные и межкомнатные <br> двери в Челябинске</div>
    	</div>
	    <div class="col-xs-12 col-md-6">
	    	<span class="call-me hidden-xs gray underline">Перезвоните мне</span>
		    <div class="white phone">8 351 750 46 47</div>
		   </div>
	  </div>
	</div>
  </header>

  <nav>
  	<div class="container">
		<ul>
			<li><a class="special-border" href="#">Каталог</a>
			<li><a class="special-border" href="#">Отзывы</a>
			<li><a class="special-border" href="#">Монтаж и доставка</a>
			<li><a class="special-border" href="#">Контактная информация</a>
			<li><a class="special-border" href="#">О компании</a>
		</ul>
	    <div class="compare">
	    	<a class="underline green">Добавлено к сравнению</a>
	    	<a class="white number">9</a>
	    </div>
	</div>
  </nav>