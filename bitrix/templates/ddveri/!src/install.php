<?php include('header.php'); ?>

<article>
	<div class="container">
		<h1 class="title">Установка дверей</h1>

		<div class="row">
			<div class="col-xs-12 col-md-8">
				<div class="entry">
					<p>Чем отличаются продавцы дверей, если в большинстве случаев двери и цены у всех одинаковые? Конечно, подходом к монтажу! Вы можете сэкономить на стоимости дверей, но мимолетное удовольствие сменят последствия недобросоветного монтажа.</p>

					<p>На качество своей работы мы даем гарантию, но к счастью, она вам не пригодится, как и не пригодилась всем нашим клиентам, двери которых вы можете посмотреть в отзывах.</p>

					<table>
						<tr>
							<th>Наименование</th>
							<th>Стоимость</th>
						</tr>
						<tr>
							<td>Установка дверей стандартная</td>
							<td>1500 Р</td>
						</tr>
						<tr>
							<td>Установка двухстворчатой двери</td>
							<td>2500 Р</td>
						</tr>
						<tr>
							<td>Установка замка до 100 мм</td>
							<td>400 Р</td>
						</tr>
						<tr>
							<td>Доборная планка до 100 мм</td>
							<td>500 Р</td>
						</tr>
						<tr>
							<td>Установка двери купе 1 створка</td>
							<td>2500 Р</td>
						</tr>
						<tr>
							<td>Установка двери купе 2 створки</td>
							<td>3500 Р</td>
						</tr>
						<tr>
							<td>Обрамление проема</td>
							<td>1500 Р</td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-xs-12 col-md-4">
				<aside>
					<form action="" class="special_form smallform">
						<div class="title">Консультация специалиста</div>
						<p>Мы перезвоним вам в течение 5 минут и поможем разобраться в наших услугах и их стоимости</p>
						<input type="hidden" name="mode" value="consult_call">
						<input type="text" class="button" name="phone" id="phone" placeholder="Ваш номер телефона">
						<input type="submit" class="button green-fill" value="Отправить">
					</form>
				</aside>
			</div>
		</div>

	</div>
</article>

<?php include('footer.php'); ?>
