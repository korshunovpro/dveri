<?php include('mobile-header.php'); ?>
<?php include('slider.php'); ?>

<div class="path">
	<div class="container">
  		<div class="row">	
	  		<div class="col-xs-12 col-sm-3 block">
	  				<img src="images/path1.png" alt="">
						<div class="title">Выбираете из каталога дверей</div>
						<div class="desc">Вы можете сравнивать двери и их характеристики с помощью ссылки «Добавить к сравнению». Чтобы заказать дверь, нажмите на соответствующую кнопку на её странице.</div>
	  		</div>
	 			
	 			<div class="delimeter col-sm-1 hidden-xs"></div>
				
				<div class="col-xs-12 col-sm-4 block">
	  				<img src="images/path2.png" alt="">
						<div class="title">Вызываете замерщика</div>
						<div class="desc">Наш сотрудник приезжает в этот же день с необходимыми каталогами, образцами и документами, производит замер и помогает вам выбрать дверь.</div>
						<div class="call-master centered-button green-fill button">Вызвать замерщика бесплатно</div>
				</div>
				
				<div class="delimeter col-sm-1 hidden-xs"></div>
	  		
	  		<div class="col-xs-12 col-sm-3 block">
	  				<img src="images/path3.png" alt="">
						<div class="title">Устанавливаем дверь</div>
						<div class="desc">В назначенное время приезжают монтажники, в течение нескольких часов устанавливают двери. После монтажа убирают за собой мусор.</div>
	  		</div>
			</div>
	</div>
</div>

<div id="catalog">
	<div class="container">
		<h1 class="title"><a href="/filter.php" class="blue underline">Межкомнатные двери</a></h1>
		<p class="description">Все модели межкомнатных дверей доступны в 3-х вариациях (одностворчатая, двустворчатая, раздвижная), фурнитураи комплектующие в стоимость не входят.</p>
		<div class="row">
		<?php for ($i=1; $i < 20; $i++) { ?>
			<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
				<div class="door">
					<div class="image">
	  				<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
	  			</div>
	  			<div class="description blue-fill top">
	  				<span class="underline name">Форпост <?php echo ($i);?></span><br>
	  				<span class="vendor">Производитель «Торекс»</span><br>
	  				<span class="price">1<?php echo ($i % 9)+1;?>000 Р</span><br>
	  				<span class="underline compare">Добавить к сравнению</span>
	  			</div>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="show-more button green-fill">Посмотреть еще 4</div>
	</div>

<div class="blue-line"></div>

	<div class="container">
		<h1 class="title"><a href="/filter.php" class="blue underline">Входные двери</a></h1>
		<p class="description">Металлические двери поставляются полностью укомплектованными (замки, ручка, глазок уплотнитель и т.д.)</p>
		<div class="row">
		<?php for ($i=1; $i < 15; $i++) { ?>
			<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
				<div class="door">
					<div class="image">
	  					<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
	  					<?php if($i == 2){ ?>
	  						<div class="mark mbonus"></div>
	  					<?php } ?>
	  					<?php if($i == 3){ ?>
	  						<div class="mark maction"></div>
	  					<?php } ?>
	  					<?php if($i == 4){ ?>
	  						<div class="mark mnew"></div>
	  					<?php } ?>
	  					<?php if($i == 1){ ?>
	  						<div class="mark msale"></div>
	  					<?php } ?>
	  				</div>
		  			<div class="description blue-fill top">
		  				<span class="underline name">Форпост <?php echo ($i);?></span><br>
		  				<span class="vendor">Производитель «Торекс»</span><br>
		  				<span class="price">1<?php echo ($i % 9)+1;?>000 Р</span><br>
		  				<span class="underline compare">Добавить к сравнению</span>
		  			</div>
				</div>
			</div>
		<?php } ?>
		</div>
		<div class="show-more button green-fill">Посмотреть еще 139</div>
	</div>
</div>

<div id="reviews">
	<div class="container">
		<h1 class="title">Последние отзывы клиентов</h1>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 review">
				<img src="images/review_photo.jpg" alt="">
				<div class="name">Джонатан Айв</div>
				<div class="bottom"><span class="date">05.07.14 /</span> <a href="mailto:jonathan-ive@apple.com" class="purple underline">jonathan-ive@apple.com</a></div>
				<div class="description">
					Те, кому когда-либо приходилось делать в квартире ремонт, наверное, обращали внимание на старые газеты, наклеенные под обоями.
					<a href="	" class="purple underline">Раскрыть полностью</a>
				</div>
				<div class="container-fluid photos">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<a href="images/review_example.jpg" data-lightbox="image-1"><img src="images/review_example.jpg" alt=""></a>
						</div>
						<div class="col-xs-12 col-md-6">
							<a href="images/review_example2.jpg" data-lightbox="image-1"><img src="images/review_example2.jpg" alt=""></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 review">
				<div class="image">
					<img src="images/review_photo.jpg" alt="">
				</div>
				<div class="name">Джонатан Айв</div>
				<div class="bottom"><span class="date">05.07.14 /</span> <a href="mailto:jonathan-ive@apple.com" class="purple underline">jonathan-ive@apple.com</a></div>
				<div class="description">
					Те, кому когда-либо приходилось делать в квартире ремонт, наверное, обращали внимание на старые газеты, наклеенные под обоями.
					<a href="	" class="purple underline">Раскрыть полностью</a>
				</div>
				<div class="container-fluid photos">
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<a href="images/review_example.jpg" data-lightbox="image-2"><img src="images/review_example.jpg" alt=""></a>
						</div>
						<div class="col-xs-12 col-md-6">
							<a href="images/review_example2.jpg" data-lightbox="image-2"><img src="images/review_example2.jpg" alt=""></a>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="show-more button green-fill">Посмотреть еще 139</div>
	</div>
</div>

<div id="prefooter">	
	<div class="container">
		<div class="row">	
			<div class="col-xs-12 col-md-6">
				<h2>Где купить дверь в Челябинске?</h2>
				<p>Как сделать выбор, если во всех магазинах представлены одни и те же двери? Можно сравнивать стоимость, но даже разница в цене не порадует вас, если дверь будет установлена не качественно. В нашем понимании установка дверей подразумевает:</p>
				<ul class="list">
					<li><span class="number">1.</span>  Высококвалифицированные, опрятные монтажники с опытом от 3-х лет.
					<li><span class="number">2.</span> Профессиональный инструмент чтобы минимизировать уровень шума, количество пыли и время монтажа.
					<li><span class="number">3.</span> Уборка мусора после монтажа (убираем за собой помещение, складываем мусор в мешки).
				</ul>
			</div>

			<div class="col-xs-12 col-md-6">
					<h2>Выставочный салон или интернет-магазин дверей?</h2>
					<p >Мы продаем двери только через интернет-магазин, что позволяет нам экономить на аренде салона и поддерживать достойные цены.</p>
					<p>В каталоге всегда присутствуют активные распродажи дверей по недорогим ценам. Мы являемся официальными дилерами большинства популярных производителей металлических и межкомнатных дверей.</p>
				  <img src="images/brands.png" alt="Наши партнеры">
			</div>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>