<?php include('header.php'); ?>

<article id="product">
	<div class="container">
		<h1 class="title">Бульдорс</h1>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<div id="path" class="blue">
						<a href="" class="blue underline">Входные двери</a> > <span class="blue underline">Бульдорс</span>
					</div>

					<div class="product-slider">
						<div class="row">
							<div class="col-xs-2 col-md-2 for-arrows"><div class="arrow left"></div></div>
							<div class="col-xs-8 col-md-8">
								<ul class="carousel">
						  		<?php for ($i=0; $i < 7; $i++) { ?>
									<div class="element">
										<div class="big-image">
						  					<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
							  			</div>
										<div class="mark mbonus"></div>
									</div>
						  		<?php } ?>
						  		</ul>
							</div>
							<div class="col-xs-2 col-md-2 for-arrows"><div class="arrow right"></div></div>
						</div>
						<div class="small-images">
							<div class="container-fluid">
								<div class="col-xs-12 col-md-12">
									<div class="product-nav">
									<?php for ($i=0; $i < 7; $i++) { ?>
										<div class="col-xs-4 col-md-2 small_image">
						  					<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="" index="<?php echo $i; ?>">
							  			</div>
							  		<?php } ?>
							  		</div>
						  		</div>
					  		</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<div class="container-fluid">
						<div class="add_to_compare hide">
							<span class="green underline">Добавить дверь <br> к сравнению</span>
						</div>
						<div class="remove_from_compare">
							<span class="purple underline">Убрать из <br> сравнения</span>
						</div>
						<div class="clearfix"></div>
						<div class="price_block">
							<div class="price">
								Цена: 12 000 Р 
							</div>
							<div class="old_price">
								Старая цена: <span class="slash">16 000 Р</span> 
							</div>
							<div class="clearfix"></div>							
							<div class="order-door button green-fill">Заказать</div>
							<div class="call_mer"><span class="call-master green underline">Вызвать замерщика бесплатно</span></div>
							<div class="clearfix"></div>
						</div>
					</div>
					<div class="colors">
						<div class="container-fluid">
						<div class="title">Расцветки</div>
						<div class="row">
							<?php for ($i=1; $i < 7; $i++) { ?>
								<div class="col-xs-2">
				  					<a href="images/color_example<?php echo ($i % 2)+1;?>.jpg" data-lightbox="colors"><img src="images/color_example<?php echo ($i % 2)+1;?>.jpg" alt=""></a>
					  			</div>
					  		<?php } ?>
						</div>
						</div>
					</div>

					<div class="features container-fluid">
						<div class="title">
							Характеристики:
						</div>
						<table>
							<tr>
								<td class="feature">Производитель:</td>
								<td>«Дверлайн шпонированные»</td>
							</tr>
							<tr>
								<td class="feature">Cтрана:</td>
								<td>Россия</td>
							</tr>
							<tr>
								<td class="feature">Покрытие:</td>
								<td>шпонированное</td>
							</tr>
							<tr>
								<td class="feature">Наличие:</td>
								<td>под заказ от 8 до 14 дней</td>
							</tr>
							<tr>
								<td class="feature">Площадь остекления:</td>
								<td>остекление отсутствует</td>
							</tr>
						</table>
					</div>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<p><b>Описание:</b></p>
					<p>Конструкция: Усиленная профильная коробка, гнутая кромка, 2 резиновых изоляционных контура.</p>
					<p>Наружная отделка: Металл с полимерно-порошковым покрытием, цвет антик-медь, 2мм</p>
					<p>Внутренняя отделка: МДФ 10мм, цвет Дуб беленый, фрезеровка «Модерн» с горизонтальными вставками Венге</p>
					<p>Осн. замок: Гардиан 3011 (Россия), сувальдный, броненакладка</p>
					<p>Доп. замок: Гардиан 3201 (Россия), цилиндровый + евроцилиндр (ключ-вертушок)</p>
					<p>Ночная задвижка: Евро</p>
					<p>Ручка: ICSA, цвет хром</p>
					<p>Утеплитель: Минеральная вата KNAUF, коробка двери утеплена</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<div class="col-xs-12 col-md-6">
						<a href="images/review_example.jpg" data-lightbox="product-images"><img src="images/review_example.jpg" alt=""></a>
					</div>
					<div class="col-xs-12 col-md-6">
						<a href="images/review_example.jpg" data-lightbox="product-images"><img src="images/review_example2.jpg" alt=""></a>
					</div>
					<div class="col-xs-12 col-md-6">
						<a href="images/review_example.jpg" data-lightbox="product-images"><img src="images/review_example.jpg" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="blue-line"></div>
	<div class="container product-info-adds">
		<div class="row">
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<h2 class="how_order">Как заказать</h2>
					<p>1. Предварительно, в удобное для вас время к вам заедет замерщик, что бы снять размеры дверного проема и, при необходимости, помочь подобрать цвет и модель двери. Для этого он вооружен каталогом и образцами покрытий.</p>
 
					<p>2. После замеров, мы договоримся с вами об оплате и времени установки. Чаще всего монтаж занимает несколько часов. За дополнительную оплату, монтажники вынесут мусор (мы, как организация не имеем право выбрасывать мусор в контейнеры, расположенные во дворах)</p>
 
					<p>Все услуги и их стоимость вы сможете посмотреть на отдельной странице</p>
					<h2 class="gurantee">Гарантии</h2>
					<p>Гарантия на дверь зависит у производителя. Уточняйте при покупке. Наша гарантия на монтаж — 1 год. Рассмотрим в течении 3-х дней</p>
 
					<p>Если по истечению гарантийного срока вам потребуется помощь в обслуживании двери, то вы можете снова воспользоваться нашими услугами за прежнюю стоимость.</p>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<div class="entry">
					<h2 class="random_review">Случайный отзыв</h2>
					<div class="container-fluid">
						<div class="row">
							<div class="review">
								<img src="images/review_photo.jpg" alt="">
								<div class="name">Джонатан Айв</div>
								<div class="bottom"><span class="date">05.07.14 /</span> <a href="mailto:jonathan-ive@apple.com" class="purple underline">jonathan-ive@apple.com</a></div>
								<div class="description">
									Те, кому когда-либо приходилось делать в квартире ремонт, наверное, обращали внимание на старые газеты, наклеенные под обоями.
									<a href="	" class="purple underline">Раскрыть полностью</a>
								</div>
								<div class="container-fluid photos">
									<div class="row">
										<div class="col-xs-12 col-md-6">
											<a href="images/review_example.jpg" data-lightbox="image-2"><img src="images/review_example.jpg" alt=""></a>
										</div>
										<div class="col-xs-12 col-md-6">
											<a href="images/review_example2.jpg" data-lightbox="image-2"><img src="images/review_example2.jpg" alt=""></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="blue-line"></div>
	
	<div id="catalog">
		<div class="container">
			<h1 class="title">Последние просмотренные</h1>
			<div class="row">
			<?php for ($i=1; $i < 5; $i++) { ?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="door">
						<div class="image">
		  				<img src="images/door_example<?php echo ($i % 4)+1;?>.jpg" alt="">
		  			</div>
		  			<div class="description blue-fill top">
		  				<span class="underline name">Форпост <?php echo ($i);?></span><br>
		  				<span class="vendor">Производитель «Торекс»</span><br>
		  				<span class="price">1<?php echo ($i % 9)+1;?>000 Р</span><br>
		  				<span class="underline compare">Добавить к сравнению</span>
		  			</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
	</div>
</article>

<div class="blue-line"></div>

<article>
	<div class="container">
		<h1 class="title">Текстовая страница</h1>
		<div class="entry">
			<p><b>Все модели межкомнатных дверей доступны в 3-х вариациях (одностворчатая, двустворчатая, раздвижная), фурнитураи комплектующие в стоимость не входят.</b></p>

			<p>Каждая студия выбирает свой стиль работы с рыбой. Автор считает, что все тексты нужно писать самому дизайнеру (правая колонка). У нас в студии принято составлять такую рыбу, чтобы она сама по себе вызывала желание принять дизайн. Поэтому очень часто рыба становится частью готового проекта — так она нравится клиенту.</p>

			<p class="quote">Текст ни о чем (пример) - хороший способ попытатсья сосредоточить заказчика на дизайне, а не на чем-то еще.</p>

			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<img src="images/review_example.jpg"  alt="">
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
					<img src="images/review_example.jpg"  alt="">
				</div>
			</div>

			<ul class="list">
				<li><span class="number">1.</span>  Высококвалифицированные, опрятные монтажники с опытом от 3-х лет.
				<li><span class="number">2.</span> Профессиональный инструмент чтобы минимизировать уровень шума, количество пыли и время монтажа.
				<li><span class="number">3.</span> Уборка мусора после монтажа (убираем за собой помещение, складываем мусор в мешки).
			</ul>
		</div>
	</div>
</article>


<?php include('footer.php'); ?>
