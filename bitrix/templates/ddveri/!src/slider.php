<div id="slider" class="main-slider slider-tab-1" style="display:none">
  <div class="container">
    <h1 class="blue">Очень длинный заголовок с «<a href="" class="purple underline">Красивым</a>» названием акции в две строки <div class="tip"><div class="purple-fill contents"><div class="title">Подсказка</div>Наш сотрудник приезжает в этот же день с необходимыми каталогами, образцами и документами, производит замер и помогает вам выбрать дверь. Наш сотрудник приезжает в этот же день 
с необходимыми каталогами.</div></div></h1>
    <div class="row">
      <div class="hidden-xs col-md-1 for-arrows"><div class="arrow left"></div></div>
      <div class="col-xs-12 col-md-10">
      <ul class="carousel">
      <?php for ($i=1; $i < 5; $i++) { ?>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 door">
          <div class="image">
            <img src="images/door<?php echo ($i % 6) + 1; ?>.jpg" alt="">
          </div>
          <div class="description top">
            <div class="white name">Форпост <?php echo ($i); ?></div>
            <div class="prices">
              <div class="part white new-price"><?php echo ($i * 100); ?> Р</div><div class="white del"> / </div> <div class="part gray old-price"><span class="slash"><?php echo ($i * 1110); ?> Р</span></div>
            </div>
          </div>
        </div>
      <?php } ?>
      </ul>
      </div>
      <div class="hidden-xs col-md-1 for-arrows"><div class="arrow right"></div></div>
    </div>
  </div>
</div>

<div id="slider" class="visible main-slider slider-tab-2" style="display:none">
  <div class="container">
    <h1 class="blue">Очень длинный заголовок с «<a href="" class="purple underline">Красивым</a>» названием акции в две строки <div class="tip"><div class="purple-fill contents"><div class="title">Подсказка</div>Наш сотрудник приезжает в этот же день с необходимыми каталогами, образцами и документами, производит замер и помогает вам выбрать дверь. Наш сотрудник приезжает в этот же день 
с необходимыми каталогами.</div></div></h1>
  	<div class="row">
  		<div class="hidden-xs col-md-1 for-arrows"><div class="arrow left"></div></div>
  		<div class="col-xs-12 col-md-10">
  		<ul class="carousel">
  		<?php for ($i=1; $i < 20; $i++) { ?>
	  		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 door">
	  			<div class="image">
	  				<img src="images/door<?php echo ($i % 6) + 1; ?>.jpg" alt="">
	  			</div>
	  			<div class="description top">
	  				<div class="white name">Форпост <?php echo ($i); ?></div>
	  				<div class="prices">
	  					<div class="part white new-price"><?php echo ($i * 100); ?> Р</div><div class="white del"> / </div> <div class="part gray old-price"><span class="slash"><?php echo ($i * 1110); ?> Р</span></div>
	  				</div>
	  			</div>
	  		</div>
  		<?php } ?>
  		</ul>
  		</div>
  		<div class="hidden-xs col-md-1 for-arrows"><div class="arrow right"></div></div>
  	</div>
  </div>
</div>

<div id="slider" class="main-slider slider-tab-3" style="display:none">
  <div class="container">
    <h1 class="blue">Очень длинный заголовок с «<a href="" class="purple underline">Красивым</a>» названием акции в две строки <div class="tip"><div class="purple-fill contents"><div class="title">Подсказка</div>Наш сотрудник приезжает в этот же день с необходимыми каталогами, образцами и документами, производит замер и помогает вам выбрать дверь. Наш сотрудник приезжает в этот же день 
с необходимыми каталогами.</div></div></h1>
    <div class="row">
      <div class="hidden-xs col-md-1 for-arrows"><div class="arrow left"></div></div>
      <div class="col-xs-12 col-md-10">
      <ul class="carousel">
      <?php for ($i=1; $i < 10; $i++) { ?>
        <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 door">
          <div class="image">
            <img src="images/door<?php echo ($i % 6) + 1; ?>.jpg" alt="">
          </div>
          <div class="description top">
            <div class="white name">Попост в две строки <?php echo ($i); ?></div>
            <div class="prices">
              <div class="part white new-price"><?php echo ($i * 100); ?> Р</div><div class="white del"> / </div> <div class="part gray old-price"><span class="slash"><?php echo ($i * 1110); ?> Р</span></div>
            </div>
          </div>
        </div>
      <?php } ?>
      </ul>
      </div>
      <div class="hidden-xs col-md-1 for-arrows"><div class="arrow right"></div></div>
    </div>
  </div>
</div>

<div class="sales-buttons">
	<div class="container">
		<div class="row">
			<div class="hidden-xs col-md-1"></div>
  		<div class="col-xs-4 col-md-2"><div id="tab-1" class="blue-fill button">Новинки</div></div>
  		<div class="col-xs-4 col-md-2"><div id="tab-2" class="blue-fill button active">Скидки</div></div>
      <div class="col-xs-4 col-md-2"><div id="tab-3" class="blue-fill button">Распродажа</div></div>
      <div class="col-xs-6 col-md-2"><div id="tab-4" class="blue-fill button">Избранное</div></div>
  		<div class="col-xs-6 col-md-2"><div id="tab-5" class="blue-fill button">Рекомендуем</div></div>
  		<div class="hidden-xs col-md-1"></div>
		</div>
	</div>
</div>
  