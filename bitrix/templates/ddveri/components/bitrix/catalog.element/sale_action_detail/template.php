<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$component->SetResultCacheKeys(array("PREVIEW_TEXT", 'DETAIL_TEXT'));

$ITEMS = $arResult["DISPLAY_PROPERTIES"]['DOORS']["LINK_ELEMENT_VALUE"];

if (!empty($ITEMS))
{
	$arSkuTemplate = array();

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
	?>
	<div class="container" id="catalog">
		<div class="row">

			<?foreach ($ITEMS as $key => $arItem) {
				$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
				$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
				$strMainID = $this->GetEditAreaId($arItem['ID']);

				$RESIZE = CFile::ResizeImageGet(
					$arItem["PREVIEW_PICTURE"],
					array('width'=>165, 'height'=>355),
					BX_RESIZE_IMAGE_EXACT,
					true
				);
				?>
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="door">
						<div class="image">
							<img src="<?=$RESIZE['src']?>" alt="<?=$arItem['NAME']?>">
						</div>
						<div class="description blue-fill top">
							<span class="underline name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></span><br>
							<span class="vendor">Производитель «<?=$arItem["DISPLAY_PROPERTIES"]['FACTORY']['NAME']?>»</span><br>
							<span class="price"><?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE']?> Р</span>
							<?if($arItem["DISPLAY_PROPERTIES"]['PRICE_OLD']['VALUE']){?>
								/ <span class="price"><?=$arItem["DISPLAY_PROPERTIES"]['PRICE_OLD']['VALUE']?> Р</span>
							<?}?><br>
							<span data-uid="<?=$arItem['ID']?>" class="underline compare">Добавить к сравнению</span>
						</div>
					</div>
				</div>

			<?}?>
		</div>
		<!--	<div class="show-more button green-fill">Посмотреть еще 4</div>-->
	</div>
<?
}
?>
<?if(!empty($arResult["DETAIL_TEXT"]) || !empty($arResult["DISPLAY_PROPERTIES"]['TIP']['VALUE'])){?>
	<div class="blue-line"></div>

	<article>
		<div class="container">
			<div class="entry">
				<h1 class="title"><?=strip_tags($arResult['PREVIEW_TEXT'])?></h1>
				<?if(!empty($arResult["DETAIL_TEXT"])){
					echo $arResult["DETAIL_TEXT"];
				} else {
					echo $arResult["DISPLAY_PROPERTIES"]['TIP'] ["DISPLAY_VALUE"];
				}?>
			</div>
		</div>
	</article>
<?}?>
