<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult["SECTIONS"] as $k=>$section) {
    $arResult['SECTIONS'][$k]['ELEMENT_COUNT'] = CIBlockElement::GetList(
        false,
        array(
            'IBLOCK_ID'=>$arParams['IBLOCK_ID'],
            'SECTION_ID'=>$section['ID'],
            'INCLUDE_SUBSECTIONS'=>'Y'
        ),
        array()
    );
}