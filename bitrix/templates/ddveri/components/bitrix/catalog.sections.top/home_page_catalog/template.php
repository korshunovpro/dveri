<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>

<div id="catalog">
<?foreach($arResult["SECTIONS"] as $arSection) {
	if(!count($arSection["ITEMS"])) continue;
	?>
	<div class="container">
		<h1 class="title"><a href="<?=$arSection["SECTION_PAGE_URL"]?>" class="blue underline"><?=$arSection["NAME"]?></a></h1>
		<p class="description"><?=$arSection["UF_PREVIEW_TEXT"]?></p>
		<div class="row">

	<?
	$cnt = 0;
	foreach($arSection["ITEMS"] as $arItem) {?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCST_ELEMENT_DELETE_CONFIRM')));

		foreach($arItem["PRODUCT_PROPERTIES"]['FACTORY']["VALUES"] as $v) {
			$FACTORY['NAME'] = $v;
		}

		$RESIZE = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]['ID'],
			array('width'=>165, 'height'=>355),
			BX_RESIZE_IMAGE_EXACT,
			true
		);
		?>
		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
			<div class="door">
				<div class="image">
					<img src="<?=$RESIZE['src']?>" alt="<?=$arItem['NAME']?>">
					<?php if($i == 2){ ?>
						<div class="mark mbonus"></div>
					<?php } ?>
					<?php if($i == 3){ ?>
						<div class="mark maction"></div>
					<?php } ?>
					<?php if($i == 4){ ?>
						<div class="mark mnew"></div>
					<?php } ?>
					<?php if($i == 1){ ?>
						<div class="mark msale"></div>
					<?php } ?>
				</div>
				<div class="description blue-fill top">
					<span class="underline name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></span><br>
					<span class="vendor">Производитель «<?=$FACTORY['NAME']?>»</span><br>
					<span class="price"><?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE']?> Р</span><br>
					<span data-uid="<?=$arItem['ID']?>" class="underline compare">Добавить к сравнению</span>
				</div>
			</div>
		</div>

	<?
	$cnt++;
	}?>
		</div>
		<?if($cnt < $arSection["ELEMENT_COUNT"]){?>
			<div class="show-more button green-fill">Посмотреть еще <?=($arSection["ELEMENT_COUNT"]-$cnt)?></div>
		<?}?>
	</div>
<?}?>
</div>