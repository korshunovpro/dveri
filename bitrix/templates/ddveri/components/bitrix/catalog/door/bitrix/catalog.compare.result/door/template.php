<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<article id="compare">

	<div class="compare-bg">
		<div class="specs">

			<div class="special">
				<div class="hidden-xs hidden-md hidden-sm point">
					<noindex>
						<?if($arResult["DIFFERENT"]) {?>
							<a class="green underline"
							   href="<?= htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=N", array("DIFFERENT"))) ?>"
							   rel="nofollow">Показывать все характеристики</a>
						<?} else {?>
							<a class="green underline"
							   href="<?= htmlspecialcharsbx($APPLICATION->GetCurPageParam("DIFFERENT=Y", array("DIFFERENT"))) ?>"
							   rel="nofollow">Только различающиеся</a>
						<?}?>
					</noindex>
				</div>
			</div>
			<?foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty) {
				$arCompare = Array();
				foreach($arResult["ITEMS"] as $arElem)
				{
					$arPropertyValue = $arElem["DISPLAY_PROPERTIES"][$code]["VALUE"];
					if(is_array($arPropertyValue))
					{
						sort($arPropertyValue);
						$arPropertyValue = implode(" / ", $arPropertyValue);
					}
					$arCompare[] = $arPropertyValue;
				}
				$diff = (count(array_unique($arCompare)) > 1 ? true : false);
				if($diff || !$arResult["DIFFERENT"]){?>
					<div class="spec"><div class="hidden-xs hidden-md hidden-sm point"><?=$arProperty['NAME']?></div></div>
				<?}?>
			<?}?>
		</div>
	</div>

	<div class="container">
		<h1 class="title">Таблица сравнения</h1>

	<div id="slider" class="compare">
		<div class="container">
			<div class="row">
				<?if(count($arResult["ITEMS"])>3) {?>
				<div class="hidden-xs hidden-md col-md-1 for-arrows"><div class="arrow left"></div></div>
				<?}?>
				<div class="col-xs-12 col-md-10">
					<div class="row">
						<div class="carousel">

							<?foreach($arResult["ITEMS"] as $arElement) {
								//var_dump($arElement);die;
								$RESIZE = CFile::ResizeImageGet(
									$arElement["PREVIEW_PICTURE"],
									array('width'=>165, 'height'=>355),
									BX_RESIZE_IMAGE_EXACT,
									true
								);
								$FACTORY = $arElement["DISPLAY_PROPERTIES"]['FACTORY']["LINK_ELEMENT_VALUE"][$arElement["DISPLAY_PROPERTIES"]['FACTORY']['VALUE']];
								?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 element">
									<div class="inner">
										<div class="door_spec">
											<div class="image">
												<img src="<?=$RESIZE['src']?>">
											</div>
											<div class="description blue-fill top">
												<a href="<?=$arElement['DETAIL_PAGE_URL']?>"><span class="underline name"><?=$arElement['NAME']?></span></a><br>
												<span class="vendor">Производитель «<?=$FACTORY['NAME']?>»</span><br>
												<span class="price"><?=$arElement['PROPERTIES']['PRICE']['VALUE']?> Р</span><br>
												<span data-uid="<?=$arElement['ID']?>" class="underline compare_del compared">Удалить из сравнения</span>
											</div>
										</div>
									</div>

									<div class="specs">
										<?foreach($arResult["SHOW_PROPERTIES"] as $code=>$arProperty) {
											$arCompare = Array();
											foreach($arResult["ITEMS"] as $arElem)
											{
												$arPropertyValue = $arElem["DISPLAY_PROPERTIES"][$code]["VALUE"];
												if(is_array($arPropertyValue))
												{
													sort($arPropertyValue);
													$arPropertyValue = implode(" / ", $arPropertyValue);
												}
												$arCompare[] = $arPropertyValue;
											}
											$diff = (count(array_unique($arCompare)) > 1 ? true : false);
											if($diff || !$arResult["DIFFERENT"]){?>
												<div class="spec"><?=(is_array($arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])? implode("/ ", $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"]): $arElement["DISPLAY_PROPERTIES"][$code]["DISPLAY_VALUE"])?></div>
											<?}?>
										<?}?>
									</div>
								</div>
							<?}?>
						</div>
					</div>
				</div>
				<?if(count($arResult["ITEMS"])>3) {?>
				<div class="hidden-xs hidden-md col-md-1 for-arrows"><div class="arrow right"></div></div>
				<?}?>
			</div>
		</div>
	</div>

</div>


</article>
