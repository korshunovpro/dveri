<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<div class="container">
	<h1 class="title"><?=$arResult["NAME"]?></h1>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="entry">
				<?$APPLICATION->IncludeComponent("bitrix:breadcrumb","breadcrumb",Array(
						"START_FROM" => "1",
						"PATH" => "",
						"SITE_ID" => "s1"
					)
				);?>

				<div class="product-slider">
					<div class="row">
						<div class="col-xs-2 col-md-2 for-arrows"><div class="arrow left"></div></div>
						<div class="col-xs-8 col-md-8">
							<ul class="carousel">
								<?php foreach ($arResult["MORE_PHOTO"] as $photo) { ?>
									<div class="element">
										<div class="big-image">
											<img src="<?=$photo['SRC']?>">
										</div>
										<div class="mark mbonus"></div>
									</div>
								<?php } ?>
							</ul>
						</div>
						<div class="col-xs-2 col-md-2 for-arrows"><div class="arrow right"></div></div>
					</div>
					<div class="small-images">
						<div class="container-fluid">
							<div class="col-xs-12 col-md-12">
								<div class="product-nav">
									<?php foreach ($arResult["MORE_PHOTO"] as $k=>$photo) { ?>
										<div class="col-xs-4 col-md-2 small_image"">
											<img src="<?=$photo['SRC']?>">
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="entry">
				<div class="container-fluid">
					<?if(DoorCompare::checkID($arResult["ID"])){?>
						<div class="remove_from_compare">
							<span class="purple underline compare compared" data-uid="<?=$arResult["ID"]?>">Убрать из <br> сравнения</span>
						</div>
					<?} else {?>
						<div class="add_to_compare">
							<span class="green underline compare" data-uid="<?=$arResult["ID"]?>">Добавить дверь <br> к сравнению</span>
						</div>
					<?}?>
					<div class="clearfix"></div>
					<div class="price_block">
						<div class="price">
							Цена: <?=$arResult["PROPERTIES"]['PRICE']['VALUE']?> Р
						</div>
						<?if(!empty($arResult["PROPERTIES"]['PRICE_OLD']['VALUE'])){?>
						<div class="old_price">
							Старая цена: <span class="slash"><?=$arResult["PROPERTIES"]['PRICE_OLD']['VALUE']?> Р</span>
						</div>
						<?}?>
						<div class="clearfix"></div>
						<div class="order-door button green-fill">Заказать</div>
						<div class="call_mer"><span class="call-master green underline">Вызвать замерщика бесплатно</span></div>
						<div class="clearfix"></div>
					</div>
				</div>
				<?if(!empty($arResult['COLORS'])){?>
				<div class="colors">
					<div class="container-fluid">
						<div class="title">Расцветки</div>
						<div class="row">
							<?php foreach($arResult['COLORS'] as $color) {
								$COLOR_RESIZE = CFile::ResizeImageGet(
									$color['PREVIEW_PICTURE'],
									array('width'=>60, 'height'=>60),
									BX_RESIZE_IMAGE_EXACT, true
								);
								?>
								<div class="col-xs-2">
									<a href="<?=$color['SRC']?>" data-lightbox="colors"><img src="<?=$COLOR_RESIZE['src']?>" alt="<?=$color['NAME']?>"></a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?}?>

				<?
				$aAvailablePropsForView = array(
					'COUNTRY',
					'STOCK',
					'MATERIAL',
					'GLASS_SIZE',
				);
				?>
				<div class="features container-fluid">
					<div class="title">
						Характеристики:
					</div>
					<table>
						<tr>
							<td class="feature">Производитель:</td>
							<td>«<?=$arResult['FACTORY']['NAME']?>»</td>
						</tr>
						<?foreach ($arResult["DISPLAY_PROPERTIES"] as $prop) {
							if(in_array($prop['CODE'], $aAvailablePropsForView) && !empty($prop['VALUE'])) {?>
								<tr>
									<td class="feature"><?=$prop['NAME']?>:</td>
									<td><?=$prop['VALUE']?></td>
								</tr>
							<?}
						}
						?>
					</table>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="entry">
				<p><b>Описание:</b></p>
				<?if(empty($arResult["DETAIL_TEXT"])){?>
					<?=$arResult["PREVIEW_TEXT"]?>
				<?} else {?>
					<?=$arResult["DETAIL_TEXT"]?>
				<?}?>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="entry">
			<?if(!empty($arResult["PROPERTIES"]['EXAMPLE']['VALUE'])) {

				foreach($arResult["PROPERTIES"]['EXAMPLE']['VALUE'] as $EXAMPLE_IMAGE_ID) {
					$FILE_SRC = CFile::GetPath($EXAMPLE_IMAGE_ID);
					$FILE_RESIZE = CFile::ResizeImageGet(
						$EXAMPLE_IMAGE_ID,
						array('width'=>210, 'height'=>140),
						BX_RESIZE_IMAGE_EXACT, true
					);?>

				<div class="col-xs-12 col-md-6">
					<a href="<?=$FILE_RESIZE['src']?>" data-lightbox="product-images"><img src="<?=$FILE_SRC?>"></a>
				</div>
				<?}
			}?>
			</div>
		</div>
	</div>
</div>
<div id="order-door" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Заказaть «<?=$arResult["NAME"]?>»</div>
		<input type="hidden" name="mode" value="orderdoor">
		<input type="hidden" name="model" value="<?=$arResult["NAME"]?>">
		<input type="hidden" name="model_id" value="<?=$arResult["ID"]?>">
		<input type="text" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<textarea name="comment" id="" cols="20" rows="4" placeholder="Комментарий"></textarea>
		<input type="submit" class="button green-fill" value="Заказать">
	</form>
</div>