<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult['ITEMS']))
{
	$arSkuTemplate = array();

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
?>
<div class="container" id="catalog">
	<div class="row">

<?foreach ($arResult['ITEMS'] as $key => $arItem) {
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$FACTORY = $arItem["DISPLAY_PROPERTIES"]['FACTORY']["LINK_ELEMENT_VALUE"][ $arItem["DISPLAY_PROPERTIES"]['FACTORY']['VALUE']];

	$RESIZE = CFile::ResizeImageGet(
		$arItem["PREVIEW_PICTURE"]['ID'],
		array('width'=>165, 'height'=>355),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	?>
	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
		<div class="door">
			<div class="image">
				<img src="<?=$RESIZE['src']?>" alt="<?=$arItem['NAME']?>">
			</div>
			<div class="description blue-fill top">
				<span class="underline name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></span><br>
				<span class="vendor">Производитель «<?=$FACTORY['NAME']?>»</span><br>
				<span class="price"><?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE']?> Р</span><br>
				<span data-uid="<?=$arItem['ID']?>" class="underline compare">Добавить к сравнению</span>
			</div>
		</div>
	</div>

<?}?>
	</div>
<!--	<div class="show-more button green-fill">Посмотреть еще 4</div>-->
</div>
<?
}
?>
<div class="blue-line"></div>

<article>
	<div class="container">
		<h1 class="title"><?=$arResult['NAME']?></h1>
		<div class="entry">
			<?=$arResult['DESCRIPTION']?>
		</div>
	</div>
</article>