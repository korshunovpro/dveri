<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$res_section = CIBlockSection::GetList(
    array(),
    array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'DEPTH_LEVEL'=>1)
);

while($sect = $res_section->GetNext()) {
    $arResult['SECTION_LIST'][] = $sect;
}