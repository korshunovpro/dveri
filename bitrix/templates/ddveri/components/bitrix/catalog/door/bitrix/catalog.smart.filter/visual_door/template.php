<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CJSCore::Init(array("fx"));

$CBXSanitizer = new CBXSanitizer;

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["TEMPLATE_THEME"].'/colors.css');
?>

<div class="container">
	<div class="col-xs-12 col-md-12" id="filter">
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
				<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>

			<div class="clearfix categories">
				<?foreach($arResult['SECTION_LIST'] as $section) {
					$active = '';
					if(stripos($APPLICATION->GetCurPage(false), $section['SECTION_PAGE_URL']) !== false) {
						$active = 'active ';
					}?>
					<a href="<?=$section['SECTION_PAGE_URL']?>" type="button" class="<?=$active?>inline-button button gray-fill"><?=$section['NAME']?></a>
				<?}?>
				<input type="text" class="blue search button" name="ds" placeholder="Поиск" <?if($_GET['ds']){?>value="<?=$CBXSanitizer->SanitizeHtml($_GET['ds'])?>"<?}?>>
			</div>
			<div class="clearfix filters<?if(isset($_REQUEST['open']) && $_REQUEST['open']=='true'){?> open<?}?>">
				<?
				$filterPropCnt = 0;
				foreach($arResult["ITEMS"] as $key=>$arItem) {?>
					<div class="clearfix"></div>
				<?if($filterPropCnt == 3) {?>
					<div class="clearfix hideBlock">
				<?}?>


					<?
					if($arItem["PROPERTY_TYPE"] == "N" ) {
						if (!$arItem["VALUES"]["MIN"]["VALUE"] || !$arItem["VALUES"]["MAX"]["VALUE"] || $arItem["VALUES"]["MIN"]["VALUE"] == $arItem["VALUES"]["MAX"]["VALUE"])
							continue;
						?>
						<?if(!$arItem["VALUES"]["MIN"]["HTML_VALUE"]) {
							$arItem["VALUES"]["MIN"]["HTML_VALUE"]=$arItem["VALUES"]["MIN"]["VALUE"];
						}
						if(!$arItem["VALUES"]["MAX"]["HTML_VALUE"]) {
							$arItem["VALUES"]["MAX"]["HTML_VALUE"]=$arItem["VALUES"]["MAX"]["VALUE"];
						}
						?>
						<input name="min_price_catalog" type="hidden" disabled value="<?=$arItem["VALUES"]["MIN"]["VALUE"]?>"/>
						<input name="max_price_catalog" type="hidden" disabled value="<?=$arItem["VALUES"]["MAX"]["VALUE"]?>"/>
						<input name="min_price_current" type="hidden" disabled value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"/>
						<input name="max_price_current" type="hidden" disabled value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"/>

						<span>Цена: </span>
						<input value="500" onchange="alert()" type="text" name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>" id="priceFrom" class="purple_rounded button" readonly>
						<input type="text" name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>" id="priceTo" class="purple_rounded button" readonly>
						<div class="slider">
							<div id="price-range">
								<div class="from"></div>
								<div class="to"></div>
							</div>
						</div>

					<?
						$filterPropCnt++;
					} elseif(!empty($arItem["VALUES"]) && !isset($arItem["PRICE"])) {?>
						<div>
							<span><?=$arItem["NAME"]?>:</span>
							<?foreach($arItem["VALUES"] as $val => $ar) {?>
								<div onclick="smartFilter.click(this)" data-fp="<?=$ar["CONTROL_ID"]?>" class="<?echo $ar["CHECKED"]? 'active ': ''?>bordered button checkboxButton"><?echo $ar["VALUE"];?></div>
								<input
									class="fakeChekbox"
									type="checkbox"
									value="<?echo $ar["HTML_VALUE"]?>"
									name="<?echo $ar["CONTROL_NAME"]?>"
									id="<?echo $ar["CONTROL_ID"]?>"
									<?echo $ar["CHECKED"]? 'checked="checked"': ''?>
									/>
								<?}?>
						</div>

					<?
						$filterPropCnt++;
					}
				}?>
			<?if($filterPropCnt>2) {?>
				</div>
			<?}?>
			</div>
			<br>
			<?
			if($filterPropCnt>3){?>
				<div class="adds_search orange underline">Расширенный поиск</div>
			<?}?>
			<input class="green-fill fix-button" type="submit" id="set_filter" name="set_filter" value="Подобрать" />
<!--			<input class="bx_filter_search_button" type="submit" id="del_filter" name="del_filter" value="--><?//=GetMessage("CT_BCSF_DEL_FILTER")?><!--" />-->

			<??>
			<div class="bx_filter_popup_result" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;top: 75px;left: 25px;right: 25px;">
				<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
				<a href="<?echo $arResult["FILTER_URL"]?>"><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
			</div>
			<??>
		</form>
	</div>


	<?
	$cheap = ' green';
	$expensive = ' purple';
//	if($arParams["ELEMENT_SORT_FIELD"] =='PROPERTY_PRICE'
//		&& $arParams["ELEMENT_SORT_ORDER"]=='ASC') {
//
//	} else {
//		$cheap = ' green';
//		$expensive = ' purple';
//	}
	?>
	<div class="col-xs-12 col-md-12 sorting">
		<div class="left">Сортировка:</div>
		<div class="right">
				<span class="expensive">
					<a href="<?=$APPLICATION->GetCurPageParam("sort=price;desc",array("sort"), false)?>" class="dashed<?=$expensive?>">Сначала дороже</a>
				</span>
				<span class="cheap">
					<a href="<?=$APPLICATION->GetCurPageParam("sort=price;asc",array("sort"), false)?>" class="dashed<?=$cheap?>">Сначала дешевле</a>
				</span>
		</div>
	</div>

</div>

<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>');
</script>