<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<?if(count($arResult["ITEMS"])>0){?>
<div class="blue-line"></div>
<div id="catalog">
	<div class="container">
		<h1 class="title">Последние просмотренные</h1>
		<div class="row">

<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	$FACTORY = $arItem["DISPLAY_PROPERTIES"]['FACTORY']["LINK_ELEMENT_VALUE"][ $arItem["DISPLAY_PROPERTIES"]['FACTORY']['VALUE']];

	$RESIZE = CFile::ResizeImageGet(
		$arItem["PREVIEW_PICTURE"]['ID'],
		array('width'=>165, 'height'=>355),
		BX_RESIZE_IMAGE_EXACT,
		true
	);
	?>
	<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
		<div class="door">
			<div class="image">
				<img src="<?=$RESIZE['src']?>" alt="<?=$arItem['NAME']?>">
			</div>
			<div class="description blue-fill top">
				<span class="underline name"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem['NAME']?></a></span><br>
				<span class="vendor">Производитель «<?=$FACTORY['NAME']?>»</span><br>
				<span class="price"><?=$arItem["DISPLAY_PROPERTIES"]['PRICE']['VALUE']?> Р</span><br>
				<span data-uid="<?=$arItem['ID']?>" class="underline compare">Добавить к сравнению</span>
			</div>
		</div>
	</div>
<?endforeach;?>

</div>
</div>
</div>
<?}?>