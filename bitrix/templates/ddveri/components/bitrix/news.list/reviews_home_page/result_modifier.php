<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult['REVIEWS_COUNT'] = CIBlockElement::GetList(
    array(),
    array('IBLOCK_ID' => $arParams['IBLOCK_ID']),
    array(),
    false,
    array('ID', 'NAME')
);