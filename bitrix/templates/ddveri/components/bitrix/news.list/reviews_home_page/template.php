<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);
?>
<div id="reviews">
	<div class="container">
		<a name="#reviews"></a>
		<h1 class="title">Последние отзывы клиентов</h1>
		<div class="row">
			<!--RestartBuffer-->
<?
$cntReview = 0;
$EXCLUDE = [];
foreach($arResult["ITEMS"] as $arItem){
	$EXCLUDE[] = $arItem['ID'];
	?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<?
	$PICTURE = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>140, 'height'=>140), BX_RESIZE_IMAGE_EXACT, true);
	$EMAIL = (!empty($arItem['PROPERTIES']['EMAIL']['VALUE'])) ? $arItem['PROPERTIES']['EMAIL']['VALUE']: false;

	if($arItem["DISPLAY_ACTIVE_FROM"]) {
		$DATE = $arItem["DISPLAY_ACTIVE_FROM"];
	} else {
		$DATE = FormatDate('d.m.Y', MakeTimeStamp($arItem["DATE_CREATE"]));
	}
	?>
	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 item-review review">
		<div class="image">
			<img src="<?=$PICTURE["src"]?>">
		</div>
		<div class="name"><?=$arItem["NAME"]?></div>
		<div class="bottom"><span class="date"><?=$DATE?><?if($EMAIL){?>/</span> <a href="mailto:<?=$EMAIL?>" class="purple underline"><?=$EMAIL?></a><?}?></div>
		<?
		$preview = false;
		$PREVIEW_TEXT[0] = $arItem["PREVIEW_TEXT"];
		if(strlen($arItem["PREVIEW_TEXT"]) > 148 ) {
			$PREVIEW_TEXT[0] = substr($arItem["PREVIEW_TEXT"], 0, 148);
			$PREVIEW_TEXT[1] = substr($arItem["PREVIEW_TEXT"], 148);
			$preview = true;
		}?>
		<div class="description">
			<?=$PREVIEW_TEXT[0]?><span class="full-review"><?=$PREVIEW_TEXT[1]?></span>
			<?if($preview){?>
				<span class="open-review purple underline">Раскрыть полностью</span>
			<?}?>
		</div>
		<?if(!empty($arItem["DISPLAY_PROPERTIES"]['PHOTOGALLERY']['FILE_VALUE'])){?>
		<div class="container-fluid photos">
			<div class="row">
				<?
				$ctn = 1;
				foreach($arItem["DISPLAY_PROPERTIES"]['PHOTOGALLERY']['FILE_VALUE'] as $PHOTO){
					$RESIZE = CFile::ResizeImageGet($PHOTO['ID'], array('width'=>263, 'height'=>175), BX_RESIZE_IMAGE_EXACT, true);
					?>
					<div class="col-xs-12 col-md-6">
						<a href="<?=$RESIZE['src']?>"
						   data-lightbox="image-2">
							<img src="<?=$PHOTO['SRC']?>">
						</a>
					</div>
				<?
					if($ctn == 2) break;
					$ctn++;
				}?>
			</div>
		</div>
		<?}?>
	</div>
<?
	$cntReview++;
}?>
			<!--RestartBuffer-->
		</div>
		<span data-ajax="/ajax/getcommentlist.php?AJAX_PAGE&COUNT=0&EXCLUDE=<?=implode(',',$EXCLUDE)?>" class="lazy show-more button green-fill">Посмотреть еще <?=($arResult['REVIEWS_COUNT']-$cntReview)?></span>
	</div>
</div>
