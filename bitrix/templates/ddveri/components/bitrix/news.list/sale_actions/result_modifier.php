<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//$arResult["ITEMS"][0]['DISPLAY_PROPERTIES']['DOORS']["LINK_ELEMENT_VALUE"] as $DOOR

$aDoorsId = array();
foreach($arResult["ITEMS"] as $k=>$ITEM) {
    foreach($ITEM['DISPLAY_PROPERTIES']['DOORS']['LINK_ELEMENT_VALUE'] as $j=>$DOOR) {
        $aDoors[] = $DOOR['ID'];
    }
}

$res = CIBlockElement::GetList(
    array(),
    array('ID'=>$aDoors),
    false, false,
    array('ID', 'PROPERTY_PRICE', 'PROPERTY_PRICE_OLD')
);

$arResult['DOOR_PROPS'] = array();
while($d = $res->GetNext()) {
    $arResult['DOORS'][$d['ID']] = $d;
}
