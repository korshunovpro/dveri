<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $k=>$arItem){
	$doorCount = count($arItem['DISPLAY_PROPERTIES']['DOORS']["LINK_ELEMENT_VALUE"]);
	?>
	<div id="slider" class="main-slider slider-tab-<?=$k?>" style="display:none">
		<div class="container">
			<h1 class="blue">
				<?=$arItem['PREVIEW_TEXT']?>
				<?if(!empty($arItem['DISPLAY_PROPERTIES']['TIP']['VALUE']["TEXT"])){?>
					<div class="tip">
						<div class="purple-fill contents">
							<div class="title">Подсказка</div>
							<?=$arItem['DISPLAY_PROPERTIES']['TIP']['VALUE']["TEXT"]?>
						</div>
					</div>
				<?}?>
			</h1>
			<div class="row">
			<?if($doorCount>4){?>
				<div class="hidden-xs col-md-1 for-arrows"><div class="arrow left"></div></div>
			<?}?>
				<div class="col-xs-12 col-md-10">
					<ul class="carousel">
						<?
						foreach($arItem['DISPLAY_PROPERTIES']['DOORS']["LINK_ELEMENT_VALUE"] as $DOOR) {

							$RESIZE = CFile::ResizeImageGet(
								$DOOR["PREVIEW_PICTURE"],
								array('width'=>108, 'height'=>248),
								BX_RESIZE_IMAGE_EXACT,
								true
							);
							?>
							<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3 door">
								<div class="image">
									<img src="<?=$RESIZE['src']?>" alt="<?=$DOOR['NAME']?>">
								</div>
								<div class="description top">
									<div class="white name"><a class="white name" href="<?=$DOOR["DETAIL_PAGE_URL"]?>"><?=$DOOR['NAME']?></a></div>
									<div class="prices">
										<div class="part white new-price"><?=$arResult['DOORS'][$DOOR['ID']]['PROPERTY_PRICE_VALUE']?> Р</div>
										<?if(!empty($arResult['DOORS'][$DOOR['ID']]['PROPERTY_PRICE_OLD_VALUE'])) {?>
											<div class="white del"> / </div>
											<div class="part gray old-price"><span class="slash"><?=$arResult['DOORS'][$DOOR['ID']]['PROPERTY_PRICE_OLD_VALUE']?> Р</span></div>
										<?}?>
									</div>
								</div>
							</div>
						<?php } ?>
					</ul>
				</div>
			<?if($doorCount>4){?>
				<div class="hidden-xs col-md-1 for-arrows"><div class="arrow right"></div></div>
			<?}?>
			</div>
		</div>
	</div>

<?}?>


<div class="sales-buttons">
	<div class="container">
		<div class="row">
			<div class="hidden-xs col-md-1"></div>
			<?foreach($arResult["ITEMS"] as $k=>$arItem){?>
				<div class="col-xs-4 col-md-2"><div id="tab-<?=$k?>" class="blue-fill button<?if($k==0){?> active<?}?>"><?=$arItem['NAME']?></div></div>
			<?}?>
			<div class="hidden-xs col-md-1"></div>
		</div>
	</div>
</div>
