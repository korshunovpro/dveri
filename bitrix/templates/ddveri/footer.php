<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!$isHome && !$isCatalog){?>
	</div><!--class="container"-->
</article>
<?}?>

<?if($isHome){?>
	<?$APPLICATION->IncludeFile(
		$APPLICATION->GetTemplatePath("includes/prefooter.php"),
		Array(),
		Array("MODE"=>"html")
	);?>
<?}?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-5">
				<h3>
					Контактная информация
				</h3>
				<ul>
					<li class="phone_icon">
						<span class="bold">
							<?$APPLICATION->IncludeFile(
								$APPLICATION->GetTemplatePath("includes/footer_phone.php"),
								Array(),
								Array("MODE"=>"text")
							);?>
						</span>
						<span class="call-me white underline">Перезвоните мне</span>
					</li>
					<li class="mail_icon">
						<a href="mailto:<?$APPLICATION->IncludeFile(
							$APPLICATION->GetTemplatePath("includes/footer_email.php"),
							Array(),
							Array("MODE"=>"text")
						);?>" class="bold white underline"><?$APPLICATION->IncludeFile(
								$APPLICATION->GetTemplatePath("includes/footer_email.php"),
								Array(),
								Array("MODE"=>"text")
							);?></a>
					</li>
					<li class="place_icon">
						<?$APPLICATION->IncludeFile(
							$APPLICATION->GetTemplatePath("includes/footer_address.php"),
							Array(),
							Array("MODE"=>"text")
						);?>
						<a href="/contacts/" class="white underline">Посмотреть на карте</a>
					</li>
					<li class="director_icon"><span class="director bold green underline">Связаться с директором</span></li>
				</ul>
			</div>
			<div class="col-xs-12 col-md-3">
				<h3>Карта сайта</h3>
				<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
	"ROOT_MENU_TYPE" => "bottom",
	"MENU_CACHE_TYPE" => "Y",
	"MENU_CACHE_TIME" => "36000000",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "left",
	"USE_EXT" => "N",
	"DELAY" => "N",
	"ALLOW_MULTI_SELECT" => "N"
	),
	false
);?>
			</div>
			<div class="col-xs-12 col-md-4">
				<form action="" class="smallform">
					<div class="after-send">
						В ближайшее время<br>с вами свяжется<br>наш специалист
					</div>
					<div class="before-send">
						<div class="bold title">Остались вопросы?</div>
						<p>Мы перезвоним в<br>течение 15 минут</p>
						<input type="hidden" name="mode" value="questions_call">
						<input type="text" class="button" name="phone" id="phone" placeholder="Введите номер телефона">
						<input type="submit" class="button green-fill" value="Отправить">
					</div>
				</form>
			</div>
		</div>
	</div>
</footer>

<div class="line bold">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-9">
				<?$APPLICATION->IncludeFile(
					$APPLICATION->GetTemplatePath("includes/footer_copy.php"),
					Array(),
					Array("MODE"=>"text")
				);?>
			</div>
			<div class="col-xs-12 col-md-3">
				<div class="right">
					Создание сайта - <a class="white underline" href="http://1jam.ru">Jam</a>
				</div>
			</div>
		</div>
	</div>
</div>

<?$APPLICATION->IncludeFile(
	$APPLICATION->GetTemplatePath("forms.php"),
	Array(),
	Array("MODE"=>"html")
);?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- jQuery Easing -->
<script src="<?=SITE_TEMPLATE_PATH?>/vendor/jquery.easing.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/vendor/lightbox.min.js"></script>
<!-- Slick plugin -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.min.js"></script>
<!-- Input Mask plugin -->
<script src="<?=SITE_TEMPLATE_PATH?>/vendor/jquery.inputmask.js" type="text/javascript"></script>
<!-- Site scripts -->
<script src="<?=SITE_TEMPLATE_PATH?>/js/scripts.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/main.js"></script>

</body>
</html>