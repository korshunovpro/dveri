<div id="call-me" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Заказ обратного звонка</div>
		<p>Оставьте свой номер телефона и мы перезвоним вам в течение 5 минут</p>
		<input type="hidden" name="mode" value="return_call">
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<input type="submit" class="button green-fill" value="Заказать звонок">
	</form>
</div>

<div id="call-master" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Вызов замерщика</div>
		<p>Оставьте свой номер телефона и мы перезвоним вам в течение 5 минут для уточнения деталей</p>
		<input type="hidden" name="mode" value="master_call">
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<input type="submit" class="button green-fill" value="Отправить">
	</form>
</div>

<div id="director" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Связаться с директором</div>
		<p>Пожалуйста, оставьте свои контактные данные, я перезвоню вам в течение рабочего дня</p>
		<input type="hidden" name="mode" value="director_call">
		<input type="text" class="button" name="name" id="name" placeholder="Ваше имя">
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<input type="submit" class="button green-fill" value="Жду звонка">
	</form>
</div>

<div id="consult" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Консультация специалиста</div>
		<p>Мы перезвоним вам в течение 5 минут и поможем разобраться в наших услугах и их стоимости</p>
		<input type="hidden" name="mode" value="consult_call">
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<input type="submit" class="button green-fill" value="Отправить">
	</form>
</div>

<div id="questions" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">Остались вопросы?</div>
		<p>Мы перезвоним вам<br>в течение 5 минут</p>
		<input type="hidden" name="mode" value="questions_call">
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<input type="submit" class="button green-fill" value="Отправить">
	</form>
</div>

<div id="helper" class="popup" style="display:none">
	<form action="" class="smallform">
		<div class="title">У вас есть жалобы или предложения?</div>
		<div class="title">Я буду рад вас выслушать и решить проблему.</div>
		<input type="hidden" name="mode" value="helper">
		<input type="text" class="button" name="name" id="name" placeholder="Ваше имя" required>
		<input type="tel" class="button" name="phone" id="phone" placeholder="Ваш номер телефона" required>
		<textarea name="comment" id="" cols="20" rows="4" placeholder="Сообщение"></textarea>
		<input type="submit" class="button green-fill" value="Отправить">
	</form>
</div>