<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'');
//$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/styles/style.css');
$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH.'/vendor/lightbox.css');

$isHome = false;
if($APPLICATION->GetCurPage(false) == '/') {
	$isHome = true;
}

$isCatalog = false;
if(stripos($APPLICATION->GetCurDir(false), '/catalog/') !== false) {
	$isCatalog = true;
}
?>
<!DOCTYPE html>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?$APPLICATION->ShowTitle()?></title>
<!-- Bootstrap -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta id="viewport" name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1">
<meta name="format-detection" content="telephone=no">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.3.7/slick.css"/>

<style>
	@import url(http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=cyrillic,latin);
	@import url(http://fonts.googleapis.com/css?family=Exo+2:100,200,400,300,200,500,600&subset=latin,cyrillic);
</style>

<!-- Stylesheet -->
<?
$APPLICATION->ShowHead();
?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<?$APPLICATION->ShowPanel();?>
<header>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 hidden-sm hidden-md hidden-lg to-mobile">
				<a href="/mobile.php" class="white underline"style="line-height:41px">Перейти в полную версию сайта?</a>
			</div>
			<div class="col-xs-12 col-md-6">
				<a class="white" href="/">
					<div class="white logo">
						<span class="text">
							<?$APPLICATION->IncludeFile(
								$APPLICATION->GetTemplatePath("includes/logo_title.php"),
								Array(),
								Array("MODE"=>"text")
							);?>
						</span>
					</div>
				</a>
				<div class="hidden-xs description">
					<?$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath("includes/logo_description.php"),
						Array(),
						Array("MODE"=>"text")
					);?>
				</div>
			</div>
			<div class="col-xs-12 col-md-6">
				<span class="call-me hidden-xs gray underline">Перезвоните мне</span>
				<div class="white phone">
					<?$APPLICATION->IncludeFile(
						$APPLICATION->GetTemplatePath("includes/header_phone.php"),
						Array(),
						Array("MODE"=>"text")
					);?>
				</div>
			</div>
		</div>
	</div>
</header>

<nav>
	<div class="container">
		<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
	"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
	"MENU_CACHE_TYPE" => "Y",	// Тип кеширования
	"MENU_CACHE_TIME" => "36000000",	// Время кеширования (сек.)
	"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
	"MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
	"MAX_LEVEL" => "1",	// Уровень вложенности меню
	"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
	"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
	),
	false
);?>
		<div class="compare">
			<a href="/compare/?DIFFERENT=N"><span class="underline green">Добавлено к сравнению</span></a>
			<span class="white number"><?=DoorCompare::getCountItem()?></span>
		</div>
	</div>
</nav>

<?if(!$isHome && !$isCatalog){?>
<article>
	<div class="container">
	<?if(!defined('NO_TITLE')){?>
		<h1 class="title"><?$APPLICATION->ShowTitle(false)?></h1>
	<?}?>
<?}?>