<div class="path">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-3 block">
                <img src="/bitrix/templates/ddveri/images/path1.png" alt="">
                <div class="title">Выбираете из каталога дверей</div>
                <div class="desc">Вы можете сравнивать двери и их характеристики с помощью ссылки «Добавить к сравнению». Чтобы заказать дверь, нажмите на соответствующую кнопку на её странице.</div>
            </div>

            <div class="delimeter col-sm-1 hidden-xs"></div>

            <div class="col-xs-12 col-sm-4 block">
                <img src="/bitrix/templates/ddveri/images/path2.png" alt="">
                <div class="title">Вызываете замерщика</div>
                <div class="desc">Наш сотрудник приезжает в этот же день с необходимыми каталогами, образцами и документами, производит замер и помогает вам выбрать дверь.</div>
                <div class="call-master centered-button green-fill button">Вызвать замерщика бесплатно</div>
            </div>

            <div class="delimeter col-sm-1 hidden-xs"></div>

            <div class="col-xs-12 col-sm-3 block">
                <img src="/bitrix/templates/ddveri/images/path3.png" alt="">
                <div class="title">Устанавливаем дверь</div>
                <div class="desc">В назначенное время приезжают монтажники, в течение нескольких часов устанавливают двери. После монтажа убирают за собой мусор.</div>
            </div>
        </div>
    </div>
</div>