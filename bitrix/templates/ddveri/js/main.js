jQuery(document).ready(function() {

    // filter price range
    $( "#price-range" ).slider({
        range: true,
        min: parseInt($('input[name="min_price_catalog"]').val()),
        max: parseInt($('input[name="max_price_catalog"]').val()),
        step: 100,
        values: [
            parseInt($('input[name="min_price_current"]').val()),
            parseInt($('input[name="max_price_current"]').val())
        ],
        slide: function( event, ui ) {
            $( "#priceFrom" ).val(ui.values[ 0 ] + ' Р');
            $( "#priceTo" ).val(ui.values[ 1 ] + ' Р');
        }
    });
    $( "#priceFrom" ).val($( "#price-range" ).slider( "values", 0 ) + ' Р');
    $( "#priceTo" ).val($( "#price-range" ).slider( "values", 1 ) + ' Р');

    //filter fake checkbox
    $('.checkboxButton').on('click', function(){
        var uid = $(this).data('fp');
        var chbox = $('#' + uid);
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            chbox.prop('checked', false);
        } else {
            $(this).addClass('active')
            chbox.prop('checked', true);
        }

    });

    // submit forms
    $('form input.button.green-fill').on('click', function(){
        var form = $(this).closest('form');
        var fromData = form.serialize();
        $.ajax({
            type: 'POST',
            url: '/ajax/form.php',
            data: fromData,
            success: function(data) {
                      $(form).find('input').attr('disabled','disabled');
                      $(form).find('input').css('opacity', '0.3');
                      $(form).find('p').hide();
                      $(form).find('.title').html(msg);
            },
            error:  function(xhr, str){}
        });
        return false;
    });

    // lazy
    $('#reviews .lazy').on('click', function(){
        var link = $(this);
        var url = link.data('ajax');
        $.ajax({
            type: 'GET',
            url: url,
            dataType:'html',
            success: function(data) {
                console.log(data);
                $('#reviews .row').html($('#reviews .row').html() + data);
                link.fadeOut();
            },
            error:  function(xhr, str){}
        });

    });

    // add search
    $('.adds_search').on('click', function(){
        $('.hideBlock').slideToggle()
    });


    $('#product span.compare').on('click', function(){
        var button = $(this);
        var uid = button.data('uid');

        if($(this).hasClass('compared')) {
            $.ajax({
                type: 'POST',
                url: '/compare/?ID=' + uid + '&action=DELETE_FROM_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.parent().attr('class', 'add_to_compare');
                            button.removeClass('compared')
                                .removeClass('purple')
                                .addClass('green')
                                .html('Добавить дверь <br> к сравнению');
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '/compare/?id=' + uid + '&action=ADD_TO_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.parent().attr('class', 'remove_from_compare');
                            button.addClass('compared')
                                .removeClass('green')
                                .addClass('purple')
                                .html('Убрать<br> из сравнения');
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        }
        return false;
    });


    $('.door .compare').on('click', function(){
        var button = $(this);
        var uid = button.data('uid');

        if($(this).hasClass('compared')) {
            $.ajax({
                type: 'POST',
                url: '/compare/?ID=' + uid + '&action=DELETE_FROM_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.removeClass('compared').html('Добавить к сравнению');
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '/compare/?id=' + uid + '&action=ADD_TO_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.addClass('compared').html('Удалить из сравнения');
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        }
        return false;
    });

    $('.door_spec .compare_del').on('click', function(){
        var button = $(this);
        var uid = button.data('uid');

        if($(this).hasClass('compared')) {
            $.ajax({
                type: 'POST',
                url: '/compare/?ID=' + uid + '&action=DELETE_FROM_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.removeClass('compared').html('Добавить к сравнению');
                            button.closest('.slick-slide').fadeOut()
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        } else {
            $.ajax({
                type: 'POST',
                url: '/compare/?id=' + uid + '&action=ADD_TO_COMPARE_RESULT',
                success: function(data) {
                    $.ajax({
                        type: 'POST',
                        url: '/ajax/compare.php',
                        success: function(data) {
                            $('.compare .white.number').text(data);
                            button.addClass('compared').html('Удалить из сравнения');
                        },
                        error:  function(xhr, str){}
                    });
                },
                error:  function(xhr, str){}
            });
        }
        return false;
    });




});
