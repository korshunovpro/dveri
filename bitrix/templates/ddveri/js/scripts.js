jQuery(document).ready(function() {

	function offsetPosition(e) { // отступ от верхнего края экрана до элемента
	  var offsetTop = 0;
	  do {offsetTop  += e.offsetTop;} while (e = e.offsetParent);
	  return offsetTop;
	}
	var aside = document.querySelector('nav'),
	    OP = offsetPosition(aside);
	if ($(window).width() > 700) {
		window.onscroll = function() {
		  aside.className = (OP < window.pageYOffset ? 'fixed' : ''); // если значение прокрутки больше отступа от верхнего края экрана до элемента, то элементу присваивается класс sticky
		}
	}

	/* Проверка карты */

	jQuery.exists = function(selector) {
	   return ($(selector).length > 0);
	}

	function checkMap() {
		if($.exists('.ymaps-map')) {
		  $('.ymaps-map').css('borderRadius','5px');
		} else {
			setTimeout(checkMap,1000);
		}
	}

	if($.exists('.map')) {
		checkMap();
	}

  max = 0;
  $('.compare .element').each(function() {
  	calc = $(this).find('.image').height() + $(this).find('.description').height() + 80;
    if(max < calc) { max = calc }
  });
  $('.compare .inner').css('height', max);
  $('.compare-bg').css('top', max + 80);
  max = 0;

  $( window ).resize(function() {
	  $('.compare .element').each(function() {
	  	calc = $(this).find('.image').height() + $(this).find('.description').height() + 80;
	    if(max < calc) { max = calc }
	  });
	  $('.compare .inner').css('height', max);
  	  $('.compare-bg').css('top', max + 80);
  	  max = 0;
  });

  maxh = 0;
  $('#catalog .door .image').each(function() {
  	calch = $(this).height();
    if(maxh < calch) { maxh = calch }
  });

  $('#catalog .door .image').each(function() {
  	if($(this).height() < maxh) {
  		$(this).css('marginTop', maxh - $(this).height());
  	}
  });

  $('.sales-buttons .button').click(function () {
    $('.sales-buttons').find('.active').removeClass('active');
    $(this).addClass('active');
    $('.main-slider').hide();
    $('.slider-'+$(this).attr('id')).fadeIn();
    $('.slider-'+$(this).attr('id')).find('.carousel').slick({
	  dots: false,
	  infinite: true,
	  speed: 300,
	  arrows:true,
	  prevArrow:'.left',
	  nextArrow:'.right',
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 5,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: false,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 4,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: false,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});
  });

  $(document).on('click', '.open-review', function () {
  	$(this).parent().find('.full-review').fadeIn();
  	$(this).fadeOut();
  });

  $('.sales-buttons .button:eq('+ Math.round($('.sales-buttons .button').length/2 - 1) +')').trigger('click');

  //$( "#price-range" ).slider({
  //  range: true,
  //  min: Math.round(5000),
  //  max: Math.round(45000),
  //  step: 1000,
  //  values: [ 5000, 45000 ],
  //  slide: function( event, ui ) {
  //    $( "#priceFrom" ).val(ui.values[ 0 ] + ' Р');
  //    $( "#priceTo" ).val(ui.values[ 1 ] + ' Р');
  //  }
  //});
  //
  //$( "#priceFrom" ).val($( "#price-range" ).slider( "values", 0 ) + ' Р');
  //$( "#priceTo" ).val($( "#price-range" ).slider( "values", 1 ) + ' Р');

  $('.compare-bg .spec:even').addClass('grey');

  $('input[name=phone]').inputmask("mask", {"mask": "8 (999) 999-9999"});

  $('.popup').click(function () {
  	if ($(event.target).closest(".popup form").length) return;
  	$(".popup").fadeOut("fast");
  	event.stopPropagation();
  });

  //$("form").submit(function() {
  //  form = $(this);
  //  var str = $(this).serialize();
  //  $.ajax({
  //    type: "POST",
  //    url: "send.php",
  //    data: str,
  //    success: function(msg) {
  //      $(form).find('input').attr('disabled','disabled');
  //      $(form).find('input').css('opacity', '0.3');
  //      $(form).find('p').hide();
  //      $(form).find('.title').html(msg);
  //    }
  //  });
  //  return false;
  //});

  $('body').on('click', '.call-me, .consult, .director, .call-master, .order-door, .helper', function () {
      var classes = $(event.target).attr("class").split(/\s/);
      console.log('#' + classes[0]);
      $('#' + classes[0]).fadeIn();
      return 0;
  });

   $('.compare .carousel').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows:true,
	  prevArrow:'.left',
	  nextArrow:'.right',
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  responsive: [
	    {
	      breakpoint: 1200,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: false,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 992,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        infinite: true,
	        dots: false,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 768,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 1,
	        arrows:true,
	        prevArrow:'.left',
	  		nextArrow:'.right'
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

   $('.product-slider .carousel').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  arrows:true,
	  prevArrow:'.left',
	  nextArrow:'.right',
	  slidesToShow: 1,
	  slidesToScroll: 1,
	});

   $('.product-nav img').click(function () {
   	  $('.product-slider .carousel').slickGoTo($(this).attr('index'));
   });

});