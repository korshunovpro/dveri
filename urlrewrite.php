<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/protivopozharnye-dveri/#",
		"RULE" => "SECTION_CODE=protivopozharnye-dveri",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
		"SORT" => "10",
	),
	array(
		"CONDITION" => "#^/mezhkomnatnye-dveri/#",
		"RULE" => "SECTION_CODE=mezhkomnatnye-dveri",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
		"SORT" => "10",
	),
	array(
		"CONDITION" => "#^/sale/([a-zA-Z0-9]+)/#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/sale/index.php",
	),
	array(
		"CONDITION" => "#^/vkhodnye-dveri/#",
		"RULE" => "SECTION_CODE=vkhodnye-dveri",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
		"SORT" => "10",
	),
	array(
		"CONDITION" => "#^/compare/#",
		"RULE" => "action=COMPARE",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
);

?>